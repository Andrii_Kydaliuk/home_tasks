"""
Task04_1_2
Write `is_palindrome` function that checks whether a string is a palindrome or not
Returns 'True' if it is palindrome, else 'False'

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).

Note:
Usage of any reversing functions is prohibited.
The function has to ignore special characters, whitespaces and different cases
Raise ValueError in case of wrong data type
"""


def is_palindrome(test_string: str) -> bool:
    if not isinstance(test_string, str):
        raise ValueError
    stripped_input_string = ''
    stripped_reversed_input_string = ''

    for i in test_string:
        if i.isalpha() or i.isdigit():
            stripped_reversed_input_string = i + stripped_reversed_input_string
            stripped_input_string += i

    return stripped_input_string.lower() == stripped_reversed_input_string.lower()


print(is_palindrome('asd'))
print(is_palindrome('asa'))
