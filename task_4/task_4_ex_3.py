"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


def split_alternative(str_to_split: str, delimiter: str = '') -> list:
    if not isinstance(str_to_split, str):
        raise ValueError

    result = []
    string = ''

    for index, item in enumerate(str_to_split):

        if item == delimiter:
            result.append(string)
            string = ''
            continue

        elif index == len(str_to_split) - 1:
            string += item
            result.append(string)
            break

        string += item

    return result



