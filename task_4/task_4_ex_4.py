"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`. 
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("no luck", [42])
["no luck"]
```
"""


def split_by_index(str_to_split: str, indexes):

    if not isinstance(str_to_split, str):
        raise ValueError

    result = []
    temp_string = ''
    sorted_indexes = []
    sorted_int_indexes = [i for i in list(dict.fromkeys(indexes)) if isinstance(i, int) and i > 0]

    for index, item in enumerate(sorted_int_indexes):
        if index == 0:
            sorted_indexes.append(item)
            prev_item = item
        elif item > prev_item:
            sorted_indexes.append(item)
            prev_item = item

    for index, item in enumerate(str_to_split):
        if index in sorted_indexes:
            result.append(temp_string)
            temp_string = ''
        elif index == len(str_to_split) - 1:
            temp_string += item
            result.append(temp_string)
            break
        temp_string += item

    return result

