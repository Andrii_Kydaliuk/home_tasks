"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    result = ''
    for i in string:
        if i == '"':
            result += '\''
        elif i == "'":
            result += '\"'
        else:
            result += i

    return result
