"""
Develop Rectangle class with following content:
    2 private fields type of float `side_a` and `side_b` (sides А and В of the rectangle);
    One constructor with two optional parameters a and b (parameters specify rectangle sides). Side А of a rectangle
    defaults to 4, side В - 3. Raise ValueError if received parameters are less than or equal to 0;
    Method `get_side_a`, returning value of the side А;
    Method `get_side_b`, returning value of the side В;
    Method `area`, calculating and returning the area value;
    Method `perimeter`, calculating and returning the perimeter value;
    Method `is_square`, checking whether current rectangle is square or not. Returns True if the shape is square and
    False in another case;
    Method `replace_sides`, swapping rectangle sides.

Develop class ArrayRectangles, in which declare:
    Private attribute `rectangle_array` (list of rectangles);
    One constructor that creates a list of rectangles with length `n` filled with `None` and that receives an
    arbitrary amount of objects of type `Rectangle` or a list of objects of type `Rectangle` (the list must be
    unpacked inside the constructor so that there will be no nested arrays). If both objects and length are passed,
    at first creates a list with received objects and then add the required number of Nones to achieve the
    desired length. If `n` is less than the number of received objects, the length of the list will be equal to the
    number of objects;
    Method `add_rectangle` that adds a rectangle of type `Rectangle` to the array on the nearest free place and
    returning True, or returning False, if there is no free space in the array;
    Method `number_max_area`, that returns order number (index) of the first rectangle with the maximum area value
    (numeration starts from zero);
    Method `number_min_perimeter`, that returns order number (index) of the first rectangle with the minimum area value
    (numeration starts from zero);
    Method `number_square`, that returns the number of squares in the array of rectangles
"""


class Rectangle:
    def __init__(self, a=4.0, b=3.0):
        if a <= 0 or b <= 0:
            raise ValueError
        self.__side_a: float = a
        self.__side_b: float = b

    def get_side_a(self):
        return self.__side_a

    def get_side_b(self):
        return self.__side_b

    def area(self):
        return self.__side_b * self.__side_a

    def perimeter(self):
        return 2 * (self.__side_b + self.__side_a)

    def is_square(self):
        if self.__side_a == self.__side_b:
            return True
        else:
            return False

    def replace_sides(self):
        storage = self.__side_a
        self.__side_a = self.__side_b
        self.__side_b = storage


class ArrayRectangles:

    def __init__(self, *args, n=0):
        self.__rectangle_array = []
        if args:
            object_count = 0
            for i in args:
                if type(i) is list:
                    self.__rectangle_array.extend(i)
                    object_count += len(i)
                else:
                    self.__rectangle_array.append(i)
                    object_count += 1
            if n > object_count:
                for i in range(n-object_count):
                    self.__rectangle_array.append(None)
        else:
            self.__rectangle_array = [None for i in range(n)]

    def add_rectangle(self, rectangle: Rectangle):
        for index, item in enumerate(self.__rectangle_array):
            if item is None:
                self.__rectangle_array[index] = rectangle
                return True
        return False

    def number_max_area(self):
        max_index = -1
        max_value = 0
        for i in range(len(self.__rectangle_array)):
            if self.__rectangle_array[i] is not None:
                area = self.__rectangle_array[i].area()
                if max_value < area:
                    max_index = i
                    max_value = area
        return max_index

    def number_min_perimeter(self):
        min_index = -1
        min_value = float("inf")
        for i in range(len(self.__rectangle_array)):
            if self.__rectangle_array[i] is not None:
                perimeter = self.__rectangle_array[i].perimeter()
                if min_value > perimeter:
                    min_index = i
                    min_value = perimeter
        return min_index

    def number_square(self):
        count = 0
        for i in self.__rectangle_array:
            if i is not None and i.is_square():
                count += 1
        return count
