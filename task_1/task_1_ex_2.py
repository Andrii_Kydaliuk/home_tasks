"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse
import math
import operator

parser = argparse.ArgumentParser(description='perform standard arithmetic operation')
parser.add_argument("function_name", help="function_name ( standard built-in function name )")
parser.add_argument("operand1", type=float, help="1st_operand")
parser.add_argument("operand2", type=float, help="2nd_operand", nargs='*')


def calculate(args):
    operand1 = args.operand1
    operand2 = args.operand2
    function_name = args.function_name

    try:
        function = getattr(math, function_name)
        return function(operand1, operand2[0]) if operand2 else function(operand1)
    except AttributeError:
        try:
            function = getattr(operator, function_name)
            return function(operand1, operand2[0]) if operand2 else function(operand1)
        except AttributeError:
            raise NotImplementedError(f'passed function {function_name} is not in built-in libraries ')


def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
