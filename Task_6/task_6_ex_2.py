"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of  the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""


class Node:
    def __init__(self, item):
        self.item = item
        self.next = None

    def get_item(self):
        return self.item

    def get_next(self):
        return self.next

    def set_item(self, item):
        self.item = item

    def set_next(self, next):
        self.next = next


class CustomListIterator:
    def __init__(self, head):
        self.cur = head

    def __iter__(self):
        return self

    def __next__(self):
        if not self.cur:
            raise StopIteration
        else:
            cur = self.cur
            self.cur = self.cur.get_next()
            return cur.get_item()


class CustomList:
    def __init__(self, *data):
        self.head = None
        self.tail = None
        self.length = 0
        if data:
            for item in data:
                self.append(item)

    def __iter__(self):
        return CustomListIterator(self.head)

    def __getitem__(self, index):
        if index < 0 or index > self.length - 1:
            raise IndexError('Index out of bound')
        cur = self.head
        for i in range(index):
            cur = cur.get_next()
        return cur.get_item()

    def __setitem__(self, index, item):
        if index < 0 or index > self.length - 1:
            raise IndexError('Index out of bound')
        cur = self.head
        for i in range(index):
            cur = cur.get_next()
        cur.set_item(item)

    def __delitem__(self, index):
        if index < 0 or index > self.length - 1:
            raise IndexError('Index out of bound')
        cur = self.head
        prev = None
        if index == 0:
            self.head = cur.get_next()
        else:
            for i in range(index):
                prev = cur
                cur = cur.get_next()
            prev.set_next(cur.get_next())
        self.length -= 1

    def __len__(self):
        return self.length

    def append(self, item):
        node = Node(item)
        if self.length:
            self.tail.set_next(node)
            self.tail = node
        else:
            self.head = node
            self.tail = node
        self.length += 1

    def add_start(self, item):
        node = Node(item)
        if self.length == 0:
            self.head = node
            self.tail = self.head
        else:
            node.set_next(self.head)
            self.head = node
        self.length += 1

    def find(self, value):
        for index, item in enumerate(self):
            if item == value:
                return index
        raise ValueError(f"value '{value}' not in list{self}")

    def remove(self, value):
        cur = self.head
        prev_node = None
        for i in range(self.length):
            if cur.get_item() == value:
                if prev_node:
                    prev_node.set_next(cur.get_next())
                    self.length -= 1
                    return
                else:
                    self.head = cur.get_next()
                    self.length -= 1
                    return
            else:
                prev_node = cur
                cur = cur.get_next()

        raise ValueError(f"value '{value}' not in list{self}")

    def clear(self):
        self.head = None
        self.length = 0

