"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
>>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""
import re


def most_common_words(file_path: str, top_words: int):
    with open(file_path, 'r') as f:
        text = f.read()
        text = text.lower()
        words_in_text = re.findall(r'\w+', text)
        word_counter = {}
        for word in words_in_text:
            if word in word_counter:
                word_counter[word] += 1
            else:
                word_counter[word] = 1
        result = sorted(word_counter, key=word_counter.get, reverse=True)
        return result[:top_words]


