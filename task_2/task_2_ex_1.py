"""
Task 2_3
You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""
import argparse


parser = argparse.ArgumentParser(description="returns the maximum weight of gold that fits into a knapsack")
parser.add_argument("-W", type=int)
parser.add_argument("-w", type=int, nargs='*')
parser.add_argument("-n", type=int)


def bounded_knapsack(weights, capacity, bars_number):
    weights_matrix = [[-1 for i in range(capacity + 1)] for j in range(bars_number + 1)]

    if bars_number == 0 or capacity == 0:
        return 0
    if weights_matrix[bars_number][capacity] != -1:
        return weights_matrix[bars_number][capacity]
    if weights[bars_number - 1] <= capacity:
        weights_matrix[bars_number][capacity] = max(
            weights[bars_number - 1] + bounded_knapsack(
                weights, capacity - weights[bars_number - 1], bars_number - 1),
            bounded_knapsack(weights, capacity, bars_number - 1))
        return weights_matrix[bars_number][capacity]
    elif weights[bars_number - 1] > capacity:
        weights_matrix[bars_number][capacity] = bounded_knapsack(weights, capacity, bars_number - 1)
        return weights_matrix[bars_number][capacity]


def main():
    args = parser.parse_args()
    if args.W <= 0 or args.n <= 0:
        raise ValueError("negative parameter value")
    for i in args.w:
        if i <= 0:
            raise ValueError("negative parameter value")

    capacity = args.W
    weights = args.w
    bars_number = args.n

    print(bounded_knapsack(weights, capacity, bars_number))


if __name__ == '__main__':
    main()
