"""
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""
import argparse


def from_roman_numerals(roman_numeral,numeral_values):
    numbers = [numeral_values[char] for char in roman_numeral]
    result = 0
    if len(numbers) > 1:
        for num1, num2 in zip(numbers, numbers[1:]):
            if num1 >= num2:
                result += num1
            else:
                result -= num1
        return result + num2
    else:
        return numbers[0]


def main():
    parser = argparse.ArgumentParser(description="converts roman numeral to arabic")
    parser.add_argument("roman_numeral", type=str)
    args = parser.parse_args()

    numeral_values = {
        'I': 1, 'V': 5, 'X': 10, 'L': 50,
        'C': 100,
    }

    for i in args.roman_numeral:
        if i not in numeral_values:
            raise ValueError(f"Invalid input: {i}")

    print(from_roman_numerals(args.roman_numeral, numeral_values))


if __name__ == "__main__":
    main()
