"""
Write a function that checks whether a string is a palindrome or not.
Return 'True' if it is a palindrome, else 'False'.

Note:
Usage of reversing functions is required.
Raise ValueError in case of wrong data type

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
"""


def is_palindrome(test_string: str) -> bool:
    if isinstance(test_string, str):
        reversed_string = ''
        stripped_test_string = ''
        for i in reversed(test_string):
            if i.isalnum():
                reversed_string += i
                stripped_test_string = i + stripped_test_string
        return stripped_test_string.lower() == reversed_string.lower()
    else:
        raise ValueError(f'passed {test_string} not a string')





